#!/usr/bin/python3

import random


# Empty list place holders
buying_persons = []
hat_names = []


# Creating the list for the names of the buying parties
while True:
    buying_persons_input = input("Please enter buying party's name, type 'end' when finished.\n")
    buying_persons.append(buying_persons_input)
    if buying_persons_input == "end":
        buying_persons.pop()
        print("All buying parties entered.\n")
        break

# Creating the list for the names "kids" going into the hat
while True:
    hat_names_input = input("Please enter the names you want in the 'hat', type 'end' when you're finished.\n")
    hat_names.append(hat_names_input)
    if hat_names_input == "end":
        hat_names.pop()
        print("All hat names have been entered.\n")
        break

print("\nThis list of buying parties is:")
print(buying_persons)
print("\nThe list of names in the 'hat' is:")
print(hat_names,"\n")
print("The buying parties matched with the hat names is as follows:")
while len(hat_names) > 0:
    try:
        for buyer in buying_persons:
            hat_draw = random.choice(hat_names)
            print(buyer, "is buying for", hat_draw)
            hat_names.remove(hat_draw)
    except IndexError:
        print("\nAll out of names in the hat before running through the list of buyers completely.")