# Christmas Name Drawer Read Me

## Purpose

As each year comes and goes, families get bigger. It starts to become unreasonable to buy gifts for all the extended famliy children.
Once you hit this point the naturual fair alternative is to draw names from a hat, for each family to buy for a random kid.

This project is purely for personal growth. It solves a need in a digital manner, rather than physically drawing names from a hat.
There are plenty of more feature complete options just a search away, but this was for me to grow my knowledge with Python and using GitLab. 