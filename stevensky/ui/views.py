from django.shortcuts import render
from django.http import HttpResponse
from .forms import DrawingForm
import subprocess
import json
import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def home_page_view(request):
    return HttpResponse("Welcome to Steve and Sky's Webapp!")


def drawing_view(request):
    if request.method == 'POST':
        form = DrawingForm()
        buyers = json.dumps(request.POST['buyers'].splitlines())
        kids = json.dumps(request.POST['kids'].splitlines())
        output = drawing_function(buyers, kids)
        # call script function, passing POST data
        return render(request,
                      'drawing.html', {
                          'form': form,
                          'output': output
                      })
    else:
        form = DrawingForm()
        return render(request, 'drawing.html', {'form': form})


def drawing_function(buyers, kids):
    print(buyers, kids)
    return subprocess.check_output(["python3",
                                    BASE_DIR + "/scripts/christmas_name_drawer.py",
                                    buyers,
                                    kids])
