from django.shortcuts import render
from django.http import HttpResponse
from .forms import DrawingForm
import subprocess


def home_page_view(request):
    return HttpResponse("Welcome to Steve and Sky's Webapp!")


def drawing_view(request):
    if request.method == 'POST':
        form = DrawingForm()
        buyers = request.POST['buyers']
        kids = request.POST['kids']
        output = drawing_function(buyers, kids)
        # call script function, passing POST data
        return render(request,
                      'drawing.html', {
                          'form': form,
                          'buyers': buyers,
                          'kids': kids,
                          'output': output
                      })
    else:
        form = DrawingForm()
        return render(request, 'drawing.html', {'form': form})


def drawing_function(buyers, kids):
    print(buyers, kids)
    return subprocess.check_output(["python3",
                                    "scripts/eqc_streams.py",
                                    buyers,
                                    kids])
