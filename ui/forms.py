from django import forms


class DrawingForm(forms.Form):
    buyers = forms.CharField(
        label='Buying aged Adults:',
        widget=forms.Textarea(
            attrs=({'placeholder': 'Names go here'})
        )
    )
    kids = forms.CharField(
        label='Children:',
        widget=forms.Textarea(
            attrs=({'placeholder': 'Names go here'})
        )
    )

    def clean(self):
        cleaned_data = super(DrawingForm, self).clean()
        stream_ids = cleaned_data.get('name')
        environment = cleaned_data.get('email')
        if not stream_ids and not environment:
            raise forms.ValidationError('You missing input(s)!')
